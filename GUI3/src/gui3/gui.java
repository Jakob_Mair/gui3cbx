/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui3;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.*;

/**
 *
 * @author Jakob
 */
public class gui extends JFrame implements ItemListener {

    private JPanel mainPanel = null;

    private JCheckBox checkbox_1;
    private JCheckBox checkbox_2;
    private JCheckBox checkbox_3;
    private JCheckBox checkbox_4;
    private JCheckBox checkbox_5;
    private JCheckBox checkbox_6;

    int cb_counter = 0;

    public gui() {
        initialiseGui();
    }

    private void initialiseGui() {

        //Panel Settings /Standart
        mainPanel = new JPanel();
        mainPanel.setLayout(null);
        this.setContentPane(mainPanel);

        this.setSize(500, 500);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setLocationRelativeTo(null);

//initialisieren && Text
        checkbox_1 = new JCheckBox();
        checkbox_1.setText("Auswahl_1");
        checkbox_2 = new JCheckBox();
        checkbox_2.setText("Auswahl_2");
        checkbox_3 = new JCheckBox();
        checkbox_3.setText("Auswahl_3");
        checkbox_4 = new JCheckBox();
        checkbox_4.setText("Auswahl_4");
        checkbox_5 = new JCheckBox();
        checkbox_5.setText("Auswahl_5");
        checkbox_6 = new JCheckBox();
        checkbox_6.setText("Auswahl_6");

        //adden
        mainPanel.add(checkbox_1);
        mainPanel.add(checkbox_2);
        mainPanel.add(checkbox_3);
        mainPanel.add(checkbox_4);
        mainPanel.add(checkbox_5);
        mainPanel.add(checkbox_6);
        
        
        
        //listeneradden
        checkbox_1.addItemListener(this);
        checkbox_2.addItemListener(this);
        checkbox_3.addItemListener(this);
        checkbox_4.addItemListener(this);
        checkbox_5.addItemListener(this);
        checkbox_6.addItemListener(this);
        
        
        
        //setBounds
        checkbox_1.setBounds(30, 30, 150, 30);
        checkbox_2.setBounds(190, 30, 150, 30);
        checkbox_3.setBounds(30, 70, 150, 30);
        checkbox_4.setBounds(190, 70, 150, 30);
        checkbox_5.setBounds(30, 110, 150, 30);
        checkbox_6.setBounds(190, 110, 150, 30);

    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == 1) {
            cb_counter++;
            if (cb_counter == 3) {

                if (!checkbox_1.isSelected()) {
                    checkbox_1.setEnabled(false);
                }
                if (!checkbox_2.isSelected()) {
                    checkbox_2.setEnabled(false);
                }
                if (!checkbox_3.isSelected()) {
                    checkbox_3.setEnabled(false);
                }
                if (!checkbox_4.isSelected()) {
                    checkbox_4.setEnabled(false);
                }
                if (!checkbox_5.isSelected()) {
                    checkbox_5.setEnabled(false);
                }
                if (!checkbox_6.isSelected()) {
                    checkbox_6.setEnabled(false);
                }
            }

        } else if (e.getStateChange() == 2) {

            cb_counter--;
            checkbox_1.setEnabled(true);
            checkbox_2.setEnabled(true);
            checkbox_3.setEnabled(true);
            checkbox_4.setEnabled(true);
            checkbox_5.setEnabled(true);
            checkbox_6.setEnabled(true);
            
        }
    }

}
